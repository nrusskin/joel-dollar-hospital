/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#ifndef __HOSPITAL_H__
#define __HOSPITAL_H__

#include <iostream>
#include "Charges.h"

class Hospital {
public:
	Hospital() {}
	void printCharges() const {
		for(size_t i = 0; i < charges.size(); ++i)
			std::cout << i << ". " << charges[i].name << " $" << charges[i].price << std::endl;
	}
	void appointCharge(PatientAccount& patient, size_t charge) const {
		patient.applyCharge(charges[charge]);
	}
	bool checkCharge(int type) const {
		return type >= 0 && type < charges.size();
	}
protected:
	void newCharge(const ChargeType& charge) {
		charges.push_back(charge);
	}
private:
	Charges charges;
	
};
#endif//__HOSPITAL_H__
