/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#include <iostream>
#include "PatientAccount.h"
#include "Hospital.h"
#include "Surgery.h"
#include "Pharmacy.h"

#define CHARGE_INVALID false

enum Menu {
	MENU_INVALID = 0,
	MENU_SELECT_SURGERY = 1,
	MENU_SELECT_MEDICATION = 2,
	MENU_PATIENT = 3,
	MENU_EXIT = 4,
	MENU_SIZE = 5
};

using namespace std;

void getMenuChoice(Menu &Choice) {
        int input = MENU_INVALID;
        cout << "Enter choice: ";
        cin >> input;
        while ( !(MENU_INVALID < input && MENU_SIZE > input))
        {
                cout << "Invalid choice.  Please re-enter: ";
                cin >> input;
        }
        Choice = Menu(input);
}

void getChargeType(const Hospital* hospital, PatientAccount& patient) {
	int type;
	
	hospital->printCharges();
	cout << "Enter choice:";
	cin >> type;
	while (hospital->checkCharge(type) == CHARGE_INVALID) {
		cout << "Invalid choice.  Please re-enter: ";
                cin >> type;
	}
	hospital->appointCharge(patient, type); 
}

void displayMeny() {
	cout << "1. Select surgery type" << endl;
	cout << "2. Select medication type" << endl;
	cout << "3. Patient check out" << endl;
	cout << "4. Exit" << endl;
}

int processMenuChoice(const Menu Choice, PatientAccount& patient, const Surgery* surgery, const Pharmacy* pharmacy) {
        switch(Choice) {
        case MENU_SELECT_SURGERY:
                getChargeType(surgery, patient);
                break;
        case MENU_SELECT_MEDICATION:
                getChargeType(pharmacy, patient);
                break;
        case MENU_PATIENT:
		patient.checkOut();
                break;
        case MENU_EXIT:
                return 1;
        default:
                break;
        }
        return 0;
}
int main() {
	PatientAccount	patient;
	Surgery		surgery;
	Pharmacy	pharmacy;
	Menu		choice;
	
        do {
                displayMeny();
                getMenuChoice(choice);
        } while (!processMenuChoice(choice, patient, &surgery, &pharmacy));

	return 0;
}
