/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#ifndef __PATIENT__ACCUONT_H__
#define __PATIENT__ACCUONT_H__

#include <iostream>
#include "Charges.h"

class PatientAccount {
public:
	PatientAccount() : days(0) {}
	void applyCharge(const ChargeType& charge) {
		charges.push_back(charge);
	}

	void checkOut() {
		double total = 0;
		std::cout << "Patient check out" << std::endl;
		for(size_t i = 0; i < charges.size(); ++i) {
			std::cout << i << ". " << charges[i].name << " $" << charges[i].price << std::endl;
			total += charges[i].price;
		}
		std::cout << "Total: $" << total << std::endl;
		charges.clear();
	}
private:
	unsigned int days;
	Charges charges;	
};
#endif//__PATIENT__ACCUONT_H__
