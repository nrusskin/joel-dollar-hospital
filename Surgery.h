/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#ifndef __SURGERY_H__
#define __SURGERY_H__

#include "Hospital.h"

class Surgery : public Hospital {
public:
	Surgery() {
		newCharge({"Thyroid Surgery", 1000.0});
		newCharge({"Open Heart Surgery", 1500.0});
		newCharge({"Achilles Tear Surgery", 200.0});
		newCharge({"Cataract Surgery", 1250.0});
		newCharge({"Carpal tunnel surgery", 700.0});
	}
};
#endif//__SURGERY_H__
