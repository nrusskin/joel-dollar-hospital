/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#ifndef __CHARGES_H__
#define __CHARGES_H__

#include <string>
#include <vector>

struct ChargeType {
	std::string name;
	double price;
};

typedef std::vector<ChargeType> Charges;

#endif//__CHARGES_H__
