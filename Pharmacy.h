/*
 * @author Nikolay Russkin <nrusskin@gmail.com
 */
#ifndef __PHARMACY_H__
#define __PHARMACY_H__

#include "Hospital.h"

class Pharmacy : public Hospital {
public:
	Pharmacy() {
		newCharge({"Morphine", 100.0});
		newCharge({"Ibuprofen", 15.0});
		newCharge({"Paracetamol", 5.0});
		newCharge({"Tetracycline", 25.0});
		newCharge({"Hydrogen peroxide", 10.0});
	}
};
#endif//__PHARMACY_H__
